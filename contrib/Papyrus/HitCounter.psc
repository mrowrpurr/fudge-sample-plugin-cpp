scriptName HitCounter extends ScriptObject hidden

HitCounter function Create(Actor target) global native

Int function GetTotalHitCounters() global native

function Increment(Int by = 1) native

Int property Count
    Int function get()
        return __GetCount()
    endFunction
endProperty
Int function __GetCount() native
