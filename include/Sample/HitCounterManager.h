#pragma once

#include <FDGE/Hopesfire.h>

#include "HitCounter.h"

namespace Sample {
    class HitCounterManager : public FDGE::Hook::SerializationHook {
    public:
        [[nodiscard]] static HitCounterManager& GetSingleton() noexcept;

        void RegisterHit(RE::Actor* target) noexcept;

        HitCounter* GetHitCounter(RE::Actor* target) const noexcept;

    protected:
        void OnNewGame() override;

        void OnGameSaved(std::ostream& out) override;

        void OnGameLoaded(std::istream& in) override;

    private:
        HitCounterManager();

        articuno_serde(ar) {
            ar <=> articuno::kv(_hitCounters, "hitCounters");
        }

        FDGE::Util::parallel_flat_hash_map<RE::Actor*,
            FDGE::Binding::Papyrus::WeakScriptObjectHandle<HitCounter>> _hitCounters;

        friend class articuno::access;
    };
}
