#pragma once

#include <FDGE/Hopesfire.h>

namespace Sample {
    class HitCounter : public FDGE::Binding::Papyrus::ScriptObject {
        ScriptType(HitCounter);

    public:
        HitCounter() = default;

        inline HitCounter(RE::Actor* target) noexcept
                : _target(target) {
        }

        inline int Increment(int by = 1) noexcept {
            return _count.fetch_add(by) + by;
        }

        [[nodiscard]] inline int Get() const noexcept {
            return _count.load();
        }

        [[nodiscard]] inline RE::Actor* GetTarget() const noexcept {
            return _target;
        }

        [[nodiscard]] inline RE::BSFixedString ToString() const noexcept override {
            return std::to_string(_count);
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_target, "target");
            ar <=> articuno::kv(_count, "count");
        }

        RE::Actor* _target{nullptr};
        std::atomic_int _count{0};

        friend class articuno::access;
    };
}
